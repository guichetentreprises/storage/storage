require([ 'jquery', 'lib/template','lib/pagination', 'lib/i18n', 'jquery-ui/widgets/draggable', 'jquery-ui/widgets/droppable'], function($, Template, pagination, i18n) {

	$( document ).ready(function() {
	    $('#pagination_ge_gq').pagination({
	        source :list,
	        renderItems :list,
	        showPagination: true
	    });
	});
});
