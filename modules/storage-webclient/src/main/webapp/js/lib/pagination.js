/**
 *
 * Options : 
 *    - source : function (pageOffset, cb) { ... }
 *           pageOffser : page offset, starting at 0
 *           cb : callback as function (data) where data is a search result object
 *       ??? instead return a deferrer
 *
 *    - renderItem : function (item, idx) { ... }
 *           item : data to display as plain object
 *           idx : item offset, starting at 0
 *        return jQuery object or plain HTML
 *
 *     - renderItems : function (container, data) { ... }
 *           container : ...
 *           data : search result object
 *
 */
define([ 'jquery' ], function ($) {

    var CTX = 'pagination.context';
    var DEFAULT_OPTS = {
        pageOffset : 0,
        renderItems : defaultRenderItems,
        showPagination : true
    };

    function goToPage(container, pageOffset) {
        if (undefined === pageOffset) {
            pageOffset = container.data('page-offset');
        }

        console.debug('go to page', pageOffset);
        container.data('page-offset', pageOffset);
        container.data(CTX).source(pageOffset, function (searchResult) {
            renderPage(container, searchResult);
        });
    }

    function renderPage(container, searchResult) {
        container.data(CTX).renderItems(container, searchResult);
        if(false !== container.data(CTX).showPagination){
            renderNavbar(container, searchResult);
        }
    }

    function defaultRenderItems(container, searchResult) {
        container.empty();
        searchResult.content.forEach(function (item, idx) {
            var row = container.data(CTX).renderItem(item, idx);
            if (row) {
                container.append(row);
            }
        });
    }

    function buildPaginationEvent(container, pageOffset) {
        return function (evt) {
            evt.preventDefault();
            goToPage(container, pageOffset);
        };
    }

    function emptyPaginationEvent(evt) {
        evt.preventDefault();
    }

    function buildPaginationButton(container, content, pageOffset) {
        var lnk = $('<a href="#"></a>').html(content);

        if (undefined !== pageOffset) {
            lnk.on('click', buildPaginationEvent(container, pageOffset));
        } else {
            lnk.on('click', emptyPaginationEvent);
        }

        return $('<li class="page_link"></li>').append(lnk);
    }

    function renderNavbar(container, searchResult) {
        console.debug('render navigation bar with', searchResult);
        var currentPage = Math.floor(searchResult.startIndex / searchResult.maxResults);
        var numberOfPages = Math.ceil(searchResult.totalResults / searchResult.maxResults);
        var navbar = $('<ul class="pagination"></ul>');
        var previousPage = Math.max(0, currentPage - 1),
            nextPage = Math.min(currentPage + 1, numberOfPages - 1),
            lastPage = numberOfPages - 1;

        navbar.append(buildPaginationButton(container, '<i class="fa fa-angle-double-left"></i>', 0 == currentPage ? undefined : 0));
        navbar.append(buildPaginationButton(container, '<i class="fa fa-angle-left"></i>', previousPage == currentPage ? undefined : previousPage));

        navbar.append(buildPaginationButton(container, currentPage + 1));

        navbar.append(buildPaginationButton(container, '<i class="fa fa-angle-right"></i>', nextPage == currentPage ? undefined : nextPage));
        navbar.append(buildPaginationButton(container, '<i class="fa fa-angle-double-right"></i>', lastPage == currentPage ? undefined : lastPage));

        container.data(CTX).navbar.empty().append(navbar);
    }

    function initialize(self, opts) {
        var ctx = $.extend({}, DEFAULT_OPTS, opts);

        if (!ctx.source) {
            console.warn('No source defined !!!');
            return;
        } else if (!ctx.renderItem && !opts.renderItems) {
            console.warn('No renderer defined !!!');
            return;
        }

        return self.each(function (idx, item) {
            var container = $(item);

            ctx.navbar = $('<nav class="page_navigation" aria-label="Page navigation"></nav>').appendTo(container.parent());
            container.data(CTX, ctx);

            goToPage(container, ctx.pageOffset);
        });
    }

    function execute(self, cmd, opts) {
        if (undefined === self.data(CTX)) {
            console.info('pagination not initialized');
            return self;
        }

        if ('refresh' === cmd) {
            goToPage(self);
        } else {
            console.info('unknown command "%s"', cmd);
        }

        return self;
    }

    return $.fn.pagination = function (cmd, opts) {
        if ($.isPlainObject(cmd)) {
            return initialize(this, cmd);
        } else {
            return execute(this, cmd, opts);
        }
    };

});
