/*!
 * Depends:
 *      lib/func/renderer.js
 *      lib/func/duration.js
 *      lib/func/sizeResume.js
 */ 
define([ 'lib/func/renderer', 'lib/func/duration', 'lib/func/sizeResume' ], function (renderer, duration, sizeResume) {
    return {
        renderer: renderer,
        duration: duration,
        sizeResume: sizeResume
    };
});
