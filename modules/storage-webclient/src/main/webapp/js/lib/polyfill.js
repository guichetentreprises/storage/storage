define([], function() {

    if (!Array.prototype.find) {
        Array.prototype.find = function(predicate) {
            if (this == null) {
                throw new TypeError('Array.prototype.find a été appelé sur null ou undefined');
            }
            if (typeof predicate !== 'function') {
                throw new TypeError('predicate doit être une fonction');
            }
            var list = Object(this);
            var length = list.length >>> 0;
            var thisArg = arguments[1];
            var value;

            for (var i = 0; i < length; i++) {
                value = list[i];
                if (predicate.call(thisArg, value, i, list)) {
                    return value;
                }
            }
            return undefined;
        };
    }

    if (!String.prototype.startsWith) {
        String.prototype.startsWith = function(searchString, position) {
            position = position || 0;
            return this.substr(position, searchString.length) === searchString;
        };
    }

    var oldObjectKeys = Object.keys;
    Object.keys = function (obj, deep) {
        if (true === deep) {
            var lst = [];
            for (var key in obj) {
                if (obj.propertyIsEnumerable(key)) {
                    lst.push(key);
                }
            }
            return lst;
        } else {
            return oldObjectKeys(obj);
        }
    };

    return {};

});