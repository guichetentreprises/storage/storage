define([ 'parsley', 'lib/i18n' ], function(parsley, i18n) {

    parsley.getErrorMessage = function getErrorMessage(constraint) {
        var catalog = parsley._validatorRegistry.catalog;
        var message;

        // Type constraints are a bit different, we have to match their requirements too to find right error message
        if ('type' === constraint.name) {
            var typeMessages = catalog.en[constraint.name] || {};
            message = i18n(typeMessages[constraint.requirements]);
        } else message = this.formatMessage(i18n(catalog.en[constraint.name]), constraint.requirements);

        return message || catalog.en.defaultMessage;
    };

    return parsley;

});
